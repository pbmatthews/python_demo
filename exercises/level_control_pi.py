# BYU ChE 436 - Process Dynamics and Control       #
# Class 14 - Case Study, Level Control              #
# PI Controller                                    #
# Simulate System with PI Controller on SP         #
####################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def tank(levels,t,pump,valve):
    h1 = max(0.0,levels[0])
    h2 = max(0.0,levels[1])
    c1 = 0.08 # inlet valve coefficient
    c2 = 0.04 # tank outlet coefficient
    dhdt1 = c1 * (1.0-valve) * pump - c2 * np.sqrt(h1)
    dhdt2 = c1 * valve * pump + c2 * np.sqrt(h1) - c2 * np.sqrt(h2)
    # overflow conditions
    if h1>=1.0 and dhdt1>0.0:
        dhdt1 = 0
    if h2>=1.0 and dhdt2>0.0:
        dhdt2 = 0
    dhdt = [dhdt1,dhdt2]
    return dhdt

# Initial conditions (levels)
h0 = [0.0,0.0]

# specify number of steps
ns = 1000
# define time points
t = np.linspace(0,ns/10,ns+1)
delta_t = t[1]-t[0]

# storage for recording values
op = np.zeros(ns+1)  # controller output
pv = np.zeros(ns+1)  # process variable
e = np.zeros(ns+1)   # error
ie = np.zeros(ns+1)  # integral of the error
dpv = np.zeros(ns+1) # derivative of the pv
P = np.zeros(ns+1)   # proportional
I = np.zeros(ns+1)   # integral
D = np.zeros(ns+1)   # derivative
sp = np.zeros(ns+1)  # set point
sp[25:] = 0.5

# FOPDT model parameters, optimized in level_control_opt
Kp = 0.8
taup = 30.0
thetap = 3.0

# PID (starting point)
tauI = max([0.1*taup,0.8*thetap])
Kc = 1/Kp * taup / (thetap+tauI)
tauD = 0.0

# Upper and Lower limits on OP
op_hi = 1.0
op_lo = 0.0

# valve position
valve = 0.0

# loop through time steps
for i in range(0,ns):
    e[i] = sp[i] - pv[i]
    if i >= 1:  # calculate starting on second cycle
        dpv[i] = (pv[i]-pv[i-1])/delta_t
        ie[i] = ie[i-1] + e[i] * delta_t
    P[i] = Kc * e[i]
    I[i] = Kc/tauI * ie[i]
    D[i] = - Kc * tauD * dpv[i]
    op[i] = op[0] + P[i] + I[i] + D[i]
    if op[i] > op_hi:  # check upper limit
        op[i] = op_hi
        ie[i] = ie[i] - e[i] * delta_t # anti-reset windup
    if op[i] < op_lo:  # check lower limit
        op[i] = op_lo
        ie[i] = ie[i] - e[i] * delta_t # anti-reset windup

    # Specify the pump and valve
    inputs = (op[i], valve)

    h = odeint(tank, h0, [0, 1], inputs)
    h0 = h[-1,:]
    # pv is final height of tank 2
    pv[i+1] = h[-1, 1]

op[ns] = op[ns-1]
ie[ns] = ie[ns-1]
P[ns] = P[ns-1]
I[ns] = I[ns-1]
D[ns] = D[ns-1]


# plot results
plt.figure(1)

plt.plot(t,sp,'k-',linewidth=2)
plt.plot(t,pv,'b--',linewidth=3)
plt.legend(['Set Point (SP)','Process Variable (PV)'],loc='best')
plt.ylabel('Process')
# plt.ylim([-0.1,12])
plt.xlabel('Time')
plt.show()