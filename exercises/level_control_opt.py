# BYU ChE 436 - Process Dynamics and Control       #
# Class 14 - Case Study, Level Control              #
# System FOPDT Model Optimization                  #
# Fit set response data to FOPDT                   #
####################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.integrate import solve_ivp
from scipy.optimize import minimize
from scipy.interpolate import interp1d

# load the data from the text file
data = np.loadtxt('step_test_data.txt', delimiter=',', skiprows=0)

# get initial conditions from data
t0 = data[0,0]
u0 = data[0,1]
yp0 = data[0,2]

t = data[:,0].T - t0
u = data[:,1].T
y = data[:,2].T

# number of steps
ns = len(t)

# create linear interpolator
uf = interp1d(t, u)

# create fopdt model
def fopdt(t, y, uf, km, taum, thetam):
    # arguments
    # y - output
    # t - time
    # uf - input linear function (for time shift)
    # Km - process gain
    # taum - process time constant
    # thetam - process dead time

    try:
        # at times less than dead time, assume input function is zero
        if (t-thetam) <= 0:
            um = uf(0.0)
        else:
            um = uf(t-thetam)
    except:
        um = u0

    # FOPDT model, tau moved to right side
    dydt = (-(y-yp0) + km * (um-u0)) / taum

    return dydt

# simulate FOPDT model with x = [km, taum, thetam]
def sim_model(x):
    # input arguments
    Km = x[0]
    Taum = max(x[1],0.001)
    Thetam = max(x[2], 0.001)

    # storage vectors
    ym = np.zeros(ns)

    # initial conditions
    ym[0] = yp0

    for i in range(0, ns-1):
        ts = [t[i], t[i+1]]
        # y1 = odeint(fopdt, ym[i], ts, args=(uf,Km,Taum,Thetam))
        y1 = solve_ivp(fopdt, ts, [ym[i]], args=(uf,Km,Taum,Thetam))

        ym[i+1] = y1.y.T[-1]
    return ym


# define objective
def objective(x):
    # simulate model
    ym = sim_model(x)

    # calculate objective
    obj = 0.0

    # penalize constraint violations
    if x[1] <= 0.0:
        obj = obj + abs(x[1])*1000.0
    if x[2] <= 0.0:
        obj = obj + abs(x[2]) * 1000.0

    # measurement error - SSE
    # for i in range(len(ym)):
    #     obj = obj + (ym[i] - y[i])**2
    obj = np.sum((ym - y) ** 2)

    return obj


# initial guesses
x0 = np.zeros(3)
x0[0] = 1.0  # Km (deg C / % open)
x0[1] = 0.5*60 #taum (minutes)
x0[2] = 0.2 #thetam (minutes)

print('Initial SSE Objective: {}'.format(objective(x0)))

# run optimizer, unbounded; default method is nelder-mead
# solution = minimize(objective, x0)

# run optimizer, bounded quadratic programming
bnds = ((0.0, 10.0), (0.0, 120.0), (0.0, 5.0))
solution = minimize(objective, x0, bounds=bnds, method='SLSQP')
x = solution.x

print('Final SSE Objective: {}'.format(objective(x)))
print('Kp: {}'.format(x[0]))
print('Taup: {}'.format(x[1]))
print('Thetap: {}'.format(x[2]))

# plot results
ym1 = sim_model(x0)
ym2 = sim_model(x)

plt.figure()
plt.subplot(2,1,1)
plt.plot(t, y, 'kx-', linewidth=2, label='Process Data')
plt.plot(t, ym1, 'b-', linewidth=2, label='Initial Guess')
plt.plot(t, ym2, 'r--', linewidth=2, label='Optimized FOPDT')
plt.ylabel('Output')
plt.legend(loc='best')

plt.subplot(2,1,2)
plt.plot(t,u,'bx-',linewidth=2)
plt.plot(t,uf(t),'r--',linewidth=2)
plt.legend(['Measured','Interpolated'], loc='best')
plt.ylabel('Input Data')
plt.show()
