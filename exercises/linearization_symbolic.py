####################################################
# BYU ChE 436 - Process Dynamics and Control       #
# Class 5 - Linearization                           #
# Example Problem 1, Analytical Solution           #
####################################################

import sympy as sp
sp.init_printing()

# define symbols
x,u = sp.symbols(['x', 'u'])

# define equations
dxdt = -x**2 + sp.sqrt(u)

print(sp.diff(dxdt, x))
print(sp.diff(dxdt, u))



